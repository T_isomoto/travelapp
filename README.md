# Trapedia

What's Trapedia
        Trapediaは海外旅行を考えている人向けの情報収集プラットフォームです．
        海外旅行のプランを立てる際，言語の違いからくる不安や安全面等の海外旅行特有の条件があると思います．
        旅行に行かれた方はその経験を投稿し情報をシェア，これから旅行に行かれる方は投稿された経験を検索しより良い旅行プランを立てましょう．

How to: 1, bundle install
        2, bundle exec rake db:migrate
        3, bundle exec rake db:seed

URL: https://trapedia.herokuapp.com