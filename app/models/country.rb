class Country < ActiveRecord::Base
  belongs_to :area
  validates :name, presence: true
  validates :area, presence: true
end
