class Report < ActiveRecord::Base
  has_many :sub_categories, ->{ order(:id) }
  has_many :favorites, dependent: :destroy
  belongs_to :user

  default_scope -> {order(created_at: :desc)}
  validates :user_id, presence: true
  validates :title, presence: true, length: {maximum: 30}
  validates :cost, presence: true, length: {maximum: 30}
  validates :safety, presence: true, length: {maximum: 30}
  validates :abroadArea, presence: true, length: {maximum: 30}
  validates :abroadCountry, presence: true, length: {maximum: 30}
  validates :purpose, presence: true, length: {maximum: 30}
  validates :content, presence: true

  mount_uploader :image, ImageUploader


  def favorited_by? user
    favorites.where(user_id: user.id).exists?
  end

end
