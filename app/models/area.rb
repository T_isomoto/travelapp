class Area < ActiveRecord::Base
  has_many :countries, ->{ order(:id) }
  validates :name, presence: true

end
