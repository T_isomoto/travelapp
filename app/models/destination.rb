class Destination < ActiveRecord::Base
  belongs_to :area
  belongs_to :country
  validates :name, presence: true
  validates :area, :area_id, presence: true
  validates :country, :country_id, presence: true
end
