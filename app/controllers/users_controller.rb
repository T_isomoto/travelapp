class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :index, :update, :delete]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  def new
    @user = User.new
  end

  def index
    @favorited_count = 0
    @users = User.paginate(page: params[:page])
  end

  def show
    fav_repo_id = []
    repo_id = []
    @reports = []
    @user = User.find(params[:id])
    # 体験談をお気に入り数でソート
    @user.reports.each do |f|
      repo_id.push([f.favorites.count, f.id])
    end
    repo_id = repo_id.sort_by{ |a,_| a }.reverse #お気に入りで降順ソート
    repo_id.each do |f|
      @reports+=Report.where(id: f[1])
    end
    # お気に入りの体験談
    @user.favorites.each do |f|
      fav_repo_id.push(f.report_id)
    end
    @favorited_reports = Report.where(id: fav_repo_id)
  end

  def favorites
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the Travel App!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to user_url
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

end
