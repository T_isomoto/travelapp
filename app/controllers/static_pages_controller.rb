class StaticPagesController < ApplicationController
  def home
    @q = Report.search(params[:q])
    if @q != nil
      @reports = @q.result(distinct: true).paginate(page: params[:page], per_page: 10)
    else
      @reports = Report.paginate(page: params[:page], per_page: 10)
    end
  end

  def help
  end

  def about
  end

end
