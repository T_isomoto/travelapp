class FavoritesController < ApplicationController
  before_action :logged_in_user, only: [:edit, :index, :update, :delete]

  def create
    if logged_in?
      @report = Report.find(params[:report_id])
      @favorite = current_user.favorites.create!(report: @report)
      @favorite.save
    else
      store_location
      flash[:denger] = "Please log in"
      redirect_to login_url
    end
  end

  def destroy
    @report = Report.find(params[:report_id])
    @favorite = current_user.favorites.find_by!(report: @report)
    @favorite.destroy
  end


end
