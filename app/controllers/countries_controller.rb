class CountriesController < ApplicationController
  def index
    area = Area.find(params[:area_id])
    render json: area.countries.select(:id, :name)
  end
end
