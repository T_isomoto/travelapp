class DestinationsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  def new
    @destination = Destination.new
  end

  def index
    @destination = Destination.all
  end

  def create
    @destination = Destination.new(destination_params)
    respond_to do |format|
      if @destination.save
        format.html { redirect_to @destination, notice: 'Item was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def set_item
  @destination = Destination.find(params[:id])
  end

def item_params
  params.require(:destination).permit(:name, :area_id, :country_id)
end

end
