require 'net/http'

class ReportsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  helper_method :sort_column, :sort_direction


  def create
    @destination = Destination.new
    @report = current_user.reports.build(report_params)
    if @report.save
      flash[:success] = "Report created!"
      redirect_to current_user
    else
      render 'new'
    end
  end

  def index
    @destination = Destination.new
    @q = Report.search(params[:q])
    if @q != nil
      @reports = @q.result(distinct: true).paginate(page: params[:page], per_page: 10)
    else
      @reports = Report.paginate(page: params[:page], per_page: 10)
    end
  end

  def show
    @report = Report.find(params[:id])
  end

  def edit
    @destination = Destination.new
    @report = Report.find(params[:id])
  end

  def new
     if logged_in?
       @destination = Destination.new
       @report = current_user.reports.build
     else
       logged_in_user
     end
  end

  def update
    @destination = Destination.new
    @report = Report.find(params[:id])
    if @report.update_attributes(report_params)
      flash[:success] = "Report updated"
      redirect_to @report
    else
      render 'edit'
    end
  end

  def destroy
    Report.find(params[:id]).destroy
    flash[:success] = "Report deleted"
    redirect_to current_user
  end

  def report_params
    params.require(:report).permit(:title, :cost, :safety, :abroadArea, :abroadCountry, :purpose, :content, :image)
  end


  private
  def destination_params
    params.require(:destination).permit(:name, :category_id, :sub_category_id)
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

  def sort_column
    Report.column_names.include?(params[:sort]) ? params[:sort] : "cost"
  end

end
