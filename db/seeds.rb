# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Destination.destroy_all
Country.destroy_all
Area.destroy_all

{'アジア' => %w(中国 韓国 インド), 'ヨーロッパ' => %w(イタリア ギリシャ), '北アメリカ' => %w(アメリカ カナダ), 'アフリカ' => %w(エジプト カメルーン)}.each do |area_name, country_names|
  area = Area.create(name: area_name)
  country_names.each do |country_name|
    area.countries.create(name: country_name)
  end
end

# [%w(北京 中国), %w(デリー インド), %w(ソウル 韓国), %w(サンフランシスコ アメリカ), %w(ローマ イタリア)].each do |destination_name, country_name|
#   country = Country.find_by_name country_name
#   Destination.create(name: destination_name, area: country.area, country: country)
# end

user1 = User.create!(name: "やまちゃん", email: "yama@a.com", password: "yamayama")
user2 = User.create!(name: "たかちゃん", email: "taka@a.com", password: "takataka")
user3 = User.create!(name: "さとっち", email: "sato@a.com", password: "satosato")


user2.reports.create(
  title:          "trip in Europe",
  cost:           50,
  safety:         1,
  abroadArea:     "ヨーロッパ",
  abroadCountry:  "イタリア",
  purpose:        "世界遺産",
  content:       "サンフランシスコに行ってきました！IT企業のメッカ，シリコンバレーに行って色々な企業を見て回りました．"
)

user2.reports.create(
  title:          "trip in Europe",
  cost:           50,
  safety:         1,
  abroadArea:     "ヨーロッパ",
  abroadCountry:  "イタリア",
  purpose:        "世界遺産",
  content:       "サンフランシスコに行ってきました！IT企業のメッカ，シリコンバレーに行って色々な企業を見て回りました．"
)

user1.reports.create(
  title:          "trip in asia",
  cost:           10,
  safety:         3,
  abroadArea:     "アジア",
  abroadCountry:  "中国",
  purpose:        "観光",
  content:       "中国に行ってきました！北京は空気が悪いと言えわれてますが，実際にそうでした．行くときはマスクを持って行きましょう"
)

user1.reports.create(
  title:          "インドバックパッカー",
  cost:           8,
  safety:         0,
  abroadArea:     "アジア",
  abroadCountry:  "インド",
  purpose:        "観光",
  content:       "インドに行ってきました！インドと言えばバックパッカーの聖地です．色々な国から色々な人が来ていて最高でした！"
)

user1.reports.create(
  title:          "Eating kimuchi",
  cost:           20,
  safety:         5,
  abroadArea:     "アジア",
  abroadCountry:  "韓国",
  purpose:        "ショッピング",
  content:       "韓国に行ってきました！ソウルでキムチを貪り汗だくの旅行となりました．"
)

user3.reports.create(
  title:          "trip in North-America",
  cost:           30,
  safety:         5,
  abroadArea:     "北アメリカ",
  abroadCountry:  "アメリカ",
  purpose:        "観光",
  content:       "サンフランシスコに行ってきました！IT企業のメッカ，シリコンバレーに行って色々な企業を見て回りました．"
)

  user2.reports.create(
    title:          "オリンピア",
    cost:           60,
    safety:         5,
    abroadArea:     "ヨーロッパ",
    abroadCountry:  "ギリシャ",
    purpose:        "世界遺産",
    content:        "オリンピック発祥の地アテネで世界遺産巡りに行きました．歴史を肌で感じ流ことができてよかったです．"
  )

user2.reports.create(
  title:          "永遠の砂漠",
  cost:           40,
  safety:         0,
  abroadArea:     "アフリカ",
  abroadCountry:  "エジプト",
  purpose:        "自然",
  content:       "あたりを見渡せば砂漠のみ，実際に熱中症で倒れている人がいました．"
)
