class RemoveAbroadPortFromReports < ActiveRecord::Migration
  def change
    remove_column :reports, :abroadPort, :string
  end
end
