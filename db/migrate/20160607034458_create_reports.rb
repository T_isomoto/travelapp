class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :cost
      t.integer :safety
      t.integer :eval
      t.text :title
      t.text :content
      t.string :abroadArea
      t.string :abroadCountry
      t.string :abroadPort
      t.string :purpose
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :reports, [:user_id, :created_at]
  end
end
